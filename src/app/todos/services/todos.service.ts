import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { TodoInterface } from "../types/todo.interface";
import { FilterEnum } from "../types/filter.enum";

@Injectable()
export class TodosService {
    todos$ = new BehaviorSubject<TodoInterface[]>([]);
/*  todos$ => only can have TodoInterface array inside
    todos$: type of Observable that represents a value
    that changes over time and emits the current
    value to its subscribers. */

    filter$ = new BehaviorSubject<FilterEnum>(FilterEnum.all);

    addTodo(text:string):void{
        const newTodo: TodoInterface = {
            id: Math.random().toString(16),
            isCompleted: false,
            text: text
        };
        
        const updatedTodos = [...this.todos$.getValue(), newTodo];
        this.todos$.next(updatedTodos);

    }

    toggleAll(isCompleted: boolean):void{
        const updatedTodos = this.todos$.getValue().map((todo) => {
            return{
                ...todo,
                isCompleted
            };
        });
        this.todos$.next(updatedTodos);
    }

    changeFilter(filterName: FilterEnum):void{
        this.filter$.next(filterName);
    }
}