import { Component, EventEmitter, Input, Output } from "@angular/core";
import { TodoInterface } from "../../types/todo.interface";

@Component({
    selector: 'app-todos-todo',
    templateUrl: './todo.component.html'
})
export class TodoComponent {
    @Input('todo') todoProps!: TodoInterface;
    @Input('isEditing') isEditingProps!: boolean;
    @Output('setEditingId') setEditingIdEvent: EventEmitter<string|null> = new EventEmitter();

    setTodoInEditMode():void{
        console.log('dbclick')
        this.setEditingIdEvent.emit(this.todoProps.id)
    }

    removeTodo():void{
        console.log('remove todo')
    }

    toggleTodo():void{
        console.log('toggle todo')

    }
}